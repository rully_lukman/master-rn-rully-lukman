android.arch.lifecycle.LiveData:
    static final int START_VERSION
android.arch.lifecycle.ReportFragment:
    46:46:static android.arch.lifecycle.ReportFragment get(android.app.Activity)
    127:128:void setProcessListener(android.arch.lifecycle.ReportFragment$ActivityInitializationListener)
android.support.design.widget.CoordinatorLayout:
    static final java.lang.String TAG
    static final int EVENT_PRE_DRAW
    static final int EVENT_NESTED_SCROLL
    static final int EVENT_VIEW_REMOVED
android.support.graphics.drawable.AndroidResources:
    static final int STYLEABLE_VECTOR_DRAWABLE_ALPHA
    static final int STYLEABLE_VECTOR_DRAWABLE_AUTO_MIRRORED
    static final int STYLEABLE_VECTOR_DRAWABLE_HEIGHT
    static final int STYLEABLE_VECTOR_DRAWABLE_NAME
    static final int STYLEABLE_VECTOR_DRAWABLE_TINT
    static final int STYLEABLE_VECTOR_DRAWABLE_TINT_MODE
    static final int STYLEABLE_VECTOR_DRAWABLE_VIEWPORT_HEIGHT
    static final int STYLEABLE_VECTOR_DRAWABLE_VIEWPORT_WIDTH
    static final int STYLEABLE_VECTOR_DRAWABLE_WIDTH
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_NAME
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_PIVOT_X
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_PIVOT_Y
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_ROTATION
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_SCALE_X
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_SCALE_Y
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_TRANSLATE_X
    static final int STYLEABLE_VECTOR_DRAWABLE_GROUP_TRANSLATE_Y
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_FILL_ALPHA
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_FILL_COLOR
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_NAME
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_PATH_DATA
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_ALPHA
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_COLOR
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_LINE_CAP
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_LINE_JOIN
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_MITER_LIMIT
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_STROKE_WIDTH
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_TRIM_PATH_END
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_TRIM_PATH_OFFSET
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_TRIM_PATH_START
    static final int STYLEABLE_VECTOR_DRAWABLE_PATH_TRIM_PATH_FILLTYPE
    static final int STYLEABLE_VECTOR_DRAWABLE_CLIP_PATH_NAME
    static final int STYLEABLE_VECTOR_DRAWABLE_CLIP_PATH_PATH_DATA
    static final int STYLEABLE_ANIMATED_VECTOR_DRAWABLE_DRAWABLE
    static final int STYLEABLE_ANIMATED_VECTOR_DRAWABLE_TARGET_ANIMATION
    static final int STYLEABLE_ANIMATED_VECTOR_DRAWABLE_TARGET_NAME
android.support.graphics.drawable.AnimatorInflaterCompat$PathDataEvaluator:
    177:179:AnimatorInflaterCompat$PathDataEvaluator(android.support.v4.graphics.PathParser$PathDataNode[])
android.support.graphics.drawable.VectorDrawableCompat:
    static final java.lang.String LOGTAG
android.support.graphics.drawable.VectorDrawableCompat$VectorDrawableCompatState:
    int[] mCachedThemeAttrs
android.support.multidex.MultiDex:
    static final java.lang.String TAG
android.support.v4.app.BackStackRecord:
    static final java.lang.String TAG
    static final int OP_NULL
    static final int OP_ADD
    static final int OP_REPLACE
    static final int OP_REMOVE
    static final int OP_HIDE
    static final int OP_SHOW
    static final int OP_DETACH
    static final int OP_ATTACH
    static final int OP_SET_PRIMARY_NAV
    static final int OP_UNSET_PRIMARY_NAV
android.support.v4.app.Fragment:
    static final int INITIALIZING
    static final int CREATED
    static final int ACTIVITY_CREATED
    static final int STARTED
    static final int RESUMED
android.support.v4.app.FragmentActivity:
    static final java.lang.String FRAGMENTS_TAG
    static final java.lang.String NEXT_CANDIDATE_REQUEST_INDEX_TAG
    static final java.lang.String ALLOCATED_REQUEST_INDICIES_TAG
    static final java.lang.String REQUEST_FRAGMENT_WHO_TAG
    static final int MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS
    static final int MSG_RESUME_PENDING
android.support.v4.app.FragmentManagerImpl:
    static final java.lang.String TAG
    static final java.lang.String TARGET_REQUEST_CODE_STATE_TAG
    static final java.lang.String TARGET_STATE_TAG
    static final java.lang.String VIEW_STATE_TAG
    static final java.lang.String USER_VISIBLE_HINT_TAG
    static final int ANIM_DUR
android.support.v4.app.INotificationSideChannel$Stub:
    static final int TRANSACTION_notify
    static final int TRANSACTION_cancel
    static final int TRANSACTION_cancelAll
android.support.v4.app.JobIntentService:
    static final java.lang.String TAG
    static final boolean DEBUG
android.support.v4.app.JobIntentService$JobServiceEngineImpl:
    static final java.lang.String TAG
    static final boolean DEBUG
android.support.v4.app.ListFragment:
    static final int INTERNAL_EMPTY_ID
    static final int INTERNAL_PROGRESS_CONTAINER_ID
    static final int INTERNAL_LIST_CONTAINER_ID
android.support.v4.app.LoaderManagerImpl:
    static final java.lang.String TAG
android.support.v4.app.NotificationCompat$Action:
    static final java.lang.String EXTRA_SHOWS_USER_INTERFACE
    static final java.lang.String EXTRA_SEMANTIC_ACTION
android.support.v4.app.NotificationCompat$CarExtender:
    static final java.lang.String EXTRA_CAR_EXTENDER
    static final java.lang.String EXTRA_INVISIBLE_ACTIONS
android.support.v4.app.NotificationCompat$MessagingStyle$Message:
    static final java.lang.String KEY_TEXT
    static final java.lang.String KEY_TIMESTAMP
    static final java.lang.String KEY_SENDER
    static final java.lang.String KEY_DATA_MIME_TYPE
    static final java.lang.String KEY_DATA_URI
    static final java.lang.String KEY_EXTRAS_BUNDLE
    static final java.lang.String KEY_PERSON
    static final java.lang.String KEY_NOTIFICATION_PERSON
android.support.v4.app.NotificationCompatJellybean:
    static final java.lang.String EXTRA_DATA_ONLY_REMOTE_INPUTS
    static final java.lang.String EXTRA_ALLOW_GENERATED_REPLIES
android.support.v4.app.NotificationManagerCompat:
    static final int MAX_SIDE_CHANNEL_SDK_VERSION
android.support.v4.content.AsyncTaskLoader:
    static final java.lang.String TAG
    static final boolean DEBUG
android.support.v4.content.LocalBroadcastManager:
    static final int MSG_EXEC_PENDING_BROADCASTS
android.support.v4.content.ModernAsyncTask:
    480:484:protected final varargs void publishProgress(java.lang.Object[])
android.support.v4.content.pm.ShortcutManagerCompat:
    static final java.lang.String ACTION_INSTALL_SHORTCUT
    static final java.lang.String INSTALL_SHORTCUT_PERMISSION
android.support.v4.content.res.GradientColorInflaterCompat:
    71:83:static android.graphics.Shader createFromXml(android.content.res.Resources,org.xmlpull.v1.XmlPullParser,android.content.res.Resources$Theme)
android.support.v4.media.AudioAttributesCompat:
    static final int FLAG_SECURE
    static final int FLAG_SCO
    static final int FLAG_BEACON
    static final int FLAG_HW_HOTWORD
    static final int FLAG_BYPASS_INTERRUPTION_POLICY
    static final int FLAG_BYPASS_MUTE
    static final int FLAG_LOW_LATENCY
    static final int FLAG_DEEP_BUFFER
    static final int FLAG_ALL
    static final int FLAG_ALL_PUBLIC
    static final int INVALID_STREAM_TYPE
    static final java.lang.String AUDIO_ATTRIBUTES_FRAMEWORKS
    static final java.lang.String AUDIO_ATTRIBUTES_USAGE
    static final java.lang.String AUDIO_ATTRIBUTES_CONTENT_TYPE
    static final java.lang.String AUDIO_ATTRIBUTES_FLAGS
    static final java.lang.String AUDIO_ATTRIBUTES_LEGACY_STREAM_TYPE
    702:702:static int toVolumeStreamType(boolean,android.support.v4.media.AudioAttributesCompat)
android.support.v4.media.MediaBrowserCompat:
    static final java.lang.String TAG
android.support.v4.media.MediaBrowserCompat$MediaBrowserImplBase:
    static final int CONNECT_STATE_DISCONNECTING
    static final int CONNECT_STATE_DISCONNECTED
    static final int CONNECT_STATE_CONNECTING
    static final int CONNECT_STATE_CONNECTED
    static final int CONNECT_STATE_SUSPENDED
android.support.v4.media.MediaBrowserCompatApi21:
    static final java.lang.String NULL_MEDIA_ITEM_ID
android.support.v4.media.MediaBrowserServiceCompat:
    static final java.lang.String TAG
    static final int RESULT_FLAG_OPTION_NOT_HANDLED
    static final int RESULT_FLAG_ON_LOAD_ITEM_NOT_IMPLEMENTED
    static final int RESULT_FLAG_ON_SEARCH_NOT_IMPLEMENTED
android.support.v4.media.MediaMetadataCompat:
    static final int METADATA_TYPE_LONG
    static final int METADATA_TYPE_TEXT
    static final int METADATA_TYPE_BITMAP
    static final int METADATA_TYPE_RATING
android.support.v4.media.MediaSessionManager:
    static final java.lang.String TAG
    98:98:android.content.Context getContext()
android.support.v4.media.ParceledListSliceAdapterApi21:
    43:49:static java.lang.Object newInstance(java.util.List)
android.support.v4.media.session.IMediaControllerCallback$Stub:
    static final int TRANSACTION_onEvent
    static final int TRANSACTION_onSessionDestroyed
    static final int TRANSACTION_onPlaybackStateChanged
    static final int TRANSACTION_onMetadataChanged
    static final int TRANSACTION_onQueueChanged
    static final int TRANSACTION_onQueueTitleChanged
    static final int TRANSACTION_onExtrasChanged
    static final int TRANSACTION_onVolumeInfoChanged
    static final int TRANSACTION_onRepeatModeChanged
    static final int TRANSACTION_onShuffleModeChangedRemoved
    static final int TRANSACTION_onCaptioningEnabledChanged
    static final int TRANSACTION_onShuffleModeChanged
    static final int TRANSACTION_onSessionReady
android.support.v4.media.session.IMediaSession$Stub:
    static final int TRANSACTION_sendCommand
    static final int TRANSACTION_sendMediaButton
    static final int TRANSACTION_registerCallbackListener
    static final int TRANSACTION_unregisterCallbackListener
    static final int TRANSACTION_isTransportControlEnabled
    static final int TRANSACTION_getPackageName
    static final int TRANSACTION_getTag
    static final int TRANSACTION_getLaunchPendingIntent
    static final int TRANSACTION_getFlags
    static final int TRANSACTION_getVolumeAttributes
    static final int TRANSACTION_adjustVolume
    static final int TRANSACTION_setVolumeTo
    static final int TRANSACTION_getMetadata
    static final int TRANSACTION_getPlaybackState
    static final int TRANSACTION_getQueue
    static final int TRANSACTION_getQueueTitle
    static final int TRANSACTION_getExtras
    static final int TRANSACTION_getRatingType
    static final int TRANSACTION_isCaptioningEnabled
    static final int TRANSACTION_getRepeatMode
    static final int TRANSACTION_isShuffleModeEnabledRemoved
    static final int TRANSACTION_getShuffleMode
    static final int TRANSACTION_addQueueItem
    static final int TRANSACTION_addQueueItemAt
    static final int TRANSACTION_removeQueueItem
    static final int TRANSACTION_removeQueueItemAt
    static final int TRANSACTION_prepare
    static final int TRANSACTION_prepareFromMediaId
    static final int TRANSACTION_prepareFromSearch
    static final int TRANSACTION_prepareFromUri
    static final int TRANSACTION_play
    static final int TRANSACTION_playFromMediaId
    static final int TRANSACTION_playFromSearch
    static final int TRANSACTION_playFromUri
    static final int TRANSACTION_skipToQueueItem
    static final int TRANSACTION_pause
    static final int TRANSACTION_stop
    static final int TRANSACTION_next
    static final int TRANSACTION_previous
    static final int TRANSACTION_fastForward
    static final int TRANSACTION_rewind
    static final int TRANSACTION_seekTo
    static final int TRANSACTION_rate
    static final int TRANSACTION_rateWithExtras
    static final int TRANSACTION_setCaptioningEnabled
    static final int TRANSACTION_setRepeatMode
    static final int TRANSACTION_setShuffleModeEnabledRemoved
    static final int TRANSACTION_setShuffleMode
    static final int TRANSACTION_sendCustomAction
android.support.v4.media.session.MediaControllerCompat:
    static final java.lang.String TAG
android.support.v4.media.session.MediaSessionCompat:
    static final java.lang.String TAG
android.support.v4.media.session.MediaSessionCompat$MediaSessionImplApi28:
    3910:3911:MediaSessionCompat$MediaSessionImplApi28(java.lang.Object)
android.support.v4.media.session.MediaSessionCompat$MediaSessionImplBase:
    static final int RCC_PLAYSTATE_NONE
android.support.v4.media.session.MediaSessionCompatApi21:
    static final java.lang.String TAG
android.support.v4.os.IResultReceiver$Stub:
    static final int TRANSACTION_send
android.support.v4.os.LocaleListHelper:
    208:250:LocaleListHelper(java.util.Locale,android.support.v4.os.LocaleListHelper)
    259:259:static android.support.v4.os.LocaleListHelper getEmptyLocaleList()
    273:281:static android.support.v4.os.LocaleListHelper forLanguageTags(java.lang.String)
    417:417:int getFirstMatchIndex(java.lang.String[])
    428:428:java.util.Locale getFirstMatchWithEnglishSupported(java.lang.String[])
    437:437:int getFirstMatchIndexWithEnglishSupported(java.util.Collection)
    445:445:int getFirstMatchIndexWithEnglishSupported(java.lang.String[])
    455:471:static boolean isPseudoLocalesOnly(java.lang.String[])
    502:524:static android.support.v4.os.LocaleListHelper getDefault()
    533:536:static android.support.v4.os.LocaleListHelper getAdjustedDefault()
    550:551:static void setDefault(android.support.v4.os.LocaleListHelper)
    564:582:static void setDefault(android.support.v4.os.LocaleListHelper,int)
android.support.v4.provider.DocumentFile:
    static final java.lang.String TAG
android.support.v4.provider.FontsContractCompat:
    static final int RESULT_CODE_PROVIDER_NOT_FOUND
    static final int RESULT_CODE_WRONG_CERTIFICATES
android.support.v4.util.PatternsCompat:
    static final java.lang.String IANA_TOP_LEVEL_DOMAINS
android.support.v4.view.AbsSavedState:
    59:60:protected AbsSavedState(android.os.Parcel)
android.support.v4.view.ViewPager:
    939:940:void smoothScrollTo(int,int)
android.support.v4.view.ViewPropertyAnimatorCompat:
    static final int LISTENER_TAG_ID
android.support.v4.widget.CursorAdapter:
    173:174:protected void init(android.content.Context,android.database.Cursor,boolean)
android.support.v4.widget.DirectedAcyclicGraph:
    200:200:int size()
android.support.v4.widget.NestedScrollView:
    static final int ANIMATED_SCROLL_GAP
    static final float MAX_SCROLL_FACTOR
android.support.v4.widget.SlidingPaneLayout:
    1244:1263:protected boolean canScroll(android.view.View,boolean,int,int,int)
android.support.v4.widget.SwipeRefreshLayout:
    static final int CIRCLE_DIAMETER
    static final int CIRCLE_DIAMETER_LARGE
android.support.v4.widget.ViewDragHelper:
    933:952:protected boolean canScroll(android.view.View,boolean,int,int,int,int)
android.support.v7.app.AlertDialog:
    static final int LAYOUT_HINT_NONE
    static final int LAYOUT_HINT_SIDE
    88:89:protected AlertDialog(android.content.Context)
    104:107:protected AlertDialog(android.content.Context,boolean,android.content.DialogInterface$OnCancelListener)
    196:197:void setButtonPanelLayoutHint(int)
android.support.v7.app.AppCompatDelegate:
    static final java.lang.String TAG
    static final int MODE_NIGHT_UNSPECIFIED
android.support.v7.app.AppCompatDelegateImpl:
    static final java.lang.String EXCEPTION_HANDLER_MESSAGE_SUFFIX
    1972:1972:android.view.ViewGroup getSubDecor()
    2124:2125:final android.support.v7.app.AppCompatDelegateImpl$AutoNightModeManager getAutoNightModeManager()
android.support.v7.app.AppCompatDelegateImpl$PanelFeatureState:
    boolean wasLastOpen
    android.os.Bundle frozenMenuState
    2406:2415:android.os.Parcelable onSaveInstanceState()
    2419:2426:void onRestoreInstanceState(android.os.Parcelable)
    2429:2433:void applyFrozenState()
android.support.v7.app.AppCompatDialog:
    45:72:protected AppCompatDialog(android.content.Context,boolean,android.content.DialogInterface$OnCancelListener)
android.support.v7.app.TwilightManager:
    60:61:static void setInstance(android.support.v7.app.TwilightManager)
android.support.v7.graphics.drawable.AnimatedStateListDrawableCompat:
    618:620:void clearMutated()
android.support.v7.graphics.drawable.DrawableContainer:
    413:414:void setCurrentIndex(int)
    639:641:void clearMutated()
android.support.v7.graphics.drawable.DrawableContainer$DrawableContainerState:
    955:956:final void clearMutated()
android.support.v7.graphics.drawable.StateListDrawable:
    255:255:android.support.v7.graphics.drawable.StateListDrawable$StateListState getStateListState()
    266:266:int getStateCount()
    278:278:int[] getStateSet(int)
    290:290:android.graphics.drawable.Drawable getStateDrawable(int)
    302:302:int getStateDrawableIndex(int[])
    322:324:void clearMutated()
android.support.v7.view.SupportMenuInflater:
    static final java.lang.String LOG_TAG
    static final int NO_ID
android.support.v7.view.menu.CascadingMenuPopup:
    static final int HORIZ_POSITION_LEFT
    static final int HORIZ_POSITION_RIGHT
    static final int SUBMENU_TIMEOUT_MS
android.support.v7.view.menu.ListMenuPresenter:
    161:161:int getItemIndexOffset()
android.support.v7.view.menu.MenuItemImpl:
    static final int NO_ICON
    233:233:java.lang.Runnable getCallback()
android.support.v7.view.menu.MenuItemWrapperICS:
    static final java.lang.String LOG_TAG
android.support.v7.widget.AbsActionBarView:
    52:53:AbsActionBarView(android.content.Context)
android.support.v7.widget.ActionMenuView:
    static final int MIN_CELL_SIZE
    static final int GENERATED_ITEM_PADDING
android.support.v7.widget.ActionMenuView$LayoutParams:
    851:853:ActionMenuView$LayoutParams(int,int,boolean)
android.support.v7.widget.ActivityChooserModel:
    static final boolean DEBUG
    static final java.lang.String TAG_HISTORICAL_RECORDS
    static final java.lang.String TAG_HISTORICAL_RECORD
    static final java.lang.String ATTRIBUTE_ACTIVITY
    static final java.lang.String ATTRIBUTE_TIME
    static final java.lang.String ATTRIBUTE_WEIGHT
android.support.v7.widget.AppCompatImageHelper:
    161:171:void setInternalImageTint(android.content.res.ColorStateList)
android.support.v7.widget.AppCompatSeekBarHelper:
    97:97:android.graphics.drawable.Drawable getTickMark()
    101:105:void setTickMarkTintList(android.content.res.ColorStateList)
    109:109:android.content.res.ColorStateList getTickMarkTintList()
    113:117:void setTickMarkTintMode(android.graphics.PorterDuff$Mode)
    121:121:android.graphics.PorterDuff$Mode getTickMarkTintMode()
android.support.v7.widget.AppCompatTextViewAutoSizeHelper:
    static final float UNSET_AUTO_SIZE_UNIFORM_CONFIGURATION_VALUE
android.support.v7.widget.ListPopupWindow:
    static final int EXPAND_LIST_TIMEOUT
    940:941:void setListItemExpandMax(int)
android.support.v7.widget.PopupMenu:
    306:309:android.widget.ListView getMenuListView()
android.support.v7.widget.SearchView:
    static final boolean DBG
    static final java.lang.String LOG_TAG
android.support.v7.widget.SuggestionsAdapter:
    static final int REFINE_NONE
    static final int REFINE_BY_ENTRY
    static final int REFINE_ALL
    static final int INVALID_INDEX
android.support.v7.widget.Toolbar$LayoutParams:
    static final int CUSTOM
    static final int SYSTEM
    static final int EXPANDED
androidx.versionedparcelable.VersionedParcel:
    1114:1115:protected static java.lang.Throwable getRootCause(java.lang.Throwable)
bolts.AndroidExecutors:
    static final long KEEP_ALIVE_TIME
com.facebook.cache.disk.DiskStorageCache:
    221:225:protected void awaitIndex()
com.facebook.common.executors.StatefulRunnable:
    protected static final int STATE_CREATED
    protected static final int STATE_STARTED
    protected static final int STATE_CANCELLED
    protected static final int STATE_FINISHED
    protected static final int STATE_FAILED
com.facebook.drawee.backends.pipeline.PipelineDraweeController:
    201:201:protected android.content.res.Resources getResources()
    371:371:protected com.facebook.common.internal.Supplier getDataSourceSupplier()
com.facebook.drawee.controller.AbstractDraweeControllerBuilder:
    440:440:protected android.content.Context getContext()
com.facebook.drawee.debug.DebugControllerOverlayDrawable:
    static final int OVERLAY_COLOR_IMAGE_OK
    static final int OVERLAY_COLOR_IMAGE_ALMOST_OK
    static final int OVERLAY_COLOR_IMAGE_NOT_OK
com.facebook.drawee.drawable.RoundedBitmapDrawable:
    113:113:android.graphics.Paint getPaint()
com.facebook.drawee.generic.WrappingUtils:
    107:110:static android.graphics.drawable.Drawable maybeWrapWithMatrix(android.graphics.drawable.Drawable,android.graphics.Matrix)
com.facebook.drawee.view.DraweeHolder:
    249:249:protected com.facebook.drawee.components.DraweeEventTracker getDraweeEventTracker()
com.facebook.imagepipeline.cache.CountingLruMap:
    37:37:synchronized java.util.ArrayList getKeys()
    42:42:synchronized java.util.ArrayList getValues()
com.facebook.imagepipeline.core.ImagePipelineConfig:
    220:221:static void resetDefaultRequestConfig()
com.facebook.imagepipeline.producers.DiskCacheWriteProducer:
    static final java.lang.String PRODUCER_NAME
com.facebook.imagepipeline.producers.DownsampleUtil:
    141:146:static int roundToPowerOfTwo(int)
com.facebook.imagepipeline.producers.JobScheduler:
    static final java.lang.String QUEUE_TIME_KEY
com.facebook.imagepipeline.producers.LocalExifThumbnailProducer:
    static final java.lang.String CREATED_THUMBNAIL
com.facebook.imagepipeline.producers.LocalVideoThumbnailProducer:
    static final java.lang.String CREATED_THUMBNAIL
com.facebook.imagepipeline.producers.NetworkFetchProducer:
    static final long TIME_BETWEEN_PARTIAL_RESULTS_MS
com.facebook.imagepipeline.producers.PostprocessedBitmapMemoryCacheProducer:
    static final java.lang.String VALUE_FOUND
com.facebook.imagepipeline.producers.PostprocessorProducer:
    static final java.lang.String POSTPROCESSOR
com.facebook.imagepipeline.producers.ProducerConstants:
    static final java.lang.String EXTRA_CACHED_VALUE_FOUND
    static final java.lang.String EXTRA_BITMAP_SIZE
    static final java.lang.String EXTRA_HAS_GOOD_QUALITY
    static final java.lang.String EXTRA_IMAGE_TYPE
    static final java.lang.String EXTRA_IS_FINAL
    static final java.lang.String EXTRA_IMAGE_FORMAT_NAME
    static final java.lang.String ENCODED_IMAGE_SIZE
    static final java.lang.String REQUESTED_IMAGE_SIZE
    static final java.lang.String SAMPLE_SIZE
com.facebook.imagepipeline.producers.ResizeAndRotateProducer:
    static final int DEFAULT_JPEG_QUALITY
    static final int MAX_JPEG_SCALE_NUMERATOR
    static final int MIN_TRANSFORM_INTERVAL_MS
com.facebook.react.ReactActivity:
    129:129:protected final com.facebook.react.ReactNativeHost getReactNativeHost()
    133:133:protected final com.facebook.react.ReactInstanceManager getReactInstanceManager()
    137:138:protected final void loadApp(java.lang.String)
com.facebook.react.ReactNativeHost:
    109:109:protected final android.app.Application getApplication()
com.facebook.react.ReactRootView:
    518:520:void simulateAttachForTesting()
com.facebook.react.devsupport.JSCHeapCapture$CaptureException:
    33:34:JSCHeapCapture$CaptureException(java.lang.String,java.lang.Throwable)
com.facebook.react.modules.core.ChoreographerCompat$FrameCallback:
    92:100:java.lang.Runnable getRunnable()
com.facebook.react.modules.core.ChoreographerCompat$FrameCallback$2:
    93:93:ChoreographerCompat$FrameCallback$2(com.facebook.react.modules.core.ChoreographerCompat$FrameCallback)
com.facebook.react.modules.dialog.AlertFragment:
    static final java.lang.String ARG_TITLE
    static final java.lang.String ARG_MESSAGE
    static final java.lang.String ARG_BUTTON_POSITIVE
    static final java.lang.String ARG_BUTTON_NEGATIVE
    static final java.lang.String ARG_BUTTON_NEUTRAL
    static final java.lang.String ARG_ITEMS
com.facebook.react.modules.storage.ReactDatabaseSupplier:
    static final java.lang.String TABLE_CATALYST
    static final java.lang.String KEY_COLUMN
    static final java.lang.String VALUE_COLUMN
    static final java.lang.String VERSION_TABLE_CREATE
com.facebook.react.turbomodule.core.TurboModuleManager:
    63:63:protected com.facebook.react.bridge.ReactApplicationContext getReactApplicationContext()
com.facebook.react.uimanager.Spacing:
    179:182:float getWithFallback(int,int)
com.facebook.react.uimanager.UIImplementation:
    143:143:com.facebook.react.uimanager.UIViewOperationQueue getUIViewOperationQueue()
com.facebook.react.uimanager.UIViewOperationQueue:
    688:690:protected void enqueueUIOperation(com.facebook.react.uimanager.UIViewOperationQueue$UIOperation)
com.facebook.react.uimanager.ViewManagersPropertyCache:
    287:301:static java.util.Map getNativePropsForView(java.lang.Class,java.lang.Class)
com.facebook.react.views.art.ARTVirtualNode:
    protected static final float MIN_OPACITY_FOR_DRAW
com.facebook.soloader.NativeLibrary:
    41:47:protected NativeLibrary(java.util.List)
com.facebook.soloader.SoLoader:
    static final java.lang.String TAG
    static final boolean DEBUG
    455:456:static void setSoFileLoader(com.facebook.soloader.SoFileLoader)
    460:466:static void resetStatus()
com.facebook.soloader.UnpackingSoSource:
    58:68:protected UnpackingSoSource(android.content.Context,java.io.File)
com.google.android.gms.common.GoogleApiAvailabilityLight:
    static final java.lang.String TRACKING_SOURCE_DIALOG
    static final java.lang.String TRACKING_SOURCE_NOTIFICATION
com.google.android.gms.common.GooglePlayServicesUtilLight:
    static final int GMS_GENERAL_ERROR_NOTIFICATION_ID
    static final int GMS_AVAILABILITY_NOTIFICATION_ID
com.google.android.gms.common.ProGuardCanary:
    static final java.lang.String CANARY
com.google.android.gms.common.api.GoogleApi:
    90:90:protected com.google.android.gms.tasks.Task disconnectService()
com.google.android.gms.common.api.ResolvingResultCallbacks:
    1:4:protected ResolvingResultCallbacks(android.app.Activity,int)
com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl:
    1:4:protected BaseImplementation$ApiMethodImpl(com.google.android.gms.common.api.Api$AnyClientKey,com.google.android.gms.common.api.GoogleApiClient)
    10:13:protected BaseImplementation$ApiMethodImpl(com.google.android.gms.common.api.internal.BasePendingResult$CallbackHandler)
com.google.android.gms.common.api.internal.BasePendingResult:
    29:37:protected BasePendingResult(com.google.android.gms.common.api.internal.BasePendingResult$CallbackHandler)
    141:143:protected final void setCancelToken(com.google.android.gms.common.internal.ICancelToken)
com.google.android.gms.common.api.internal.DataHolderNotifier:
    1:3:protected DataHolderNotifier(com.google.android.gms.common.data.DataHolder)
com.google.android.gms.common.api.internal.DataHolderResult:
    1:2:protected DataHolderResult(com.google.android.gms.common.data.DataHolder)
    3:6:protected DataHolderResult(com.google.android.gms.common.data.DataHolder,com.google.android.gms.common.api.Status)
com.google.android.gms.common.api.internal.GoogleServices:
    53:55:static void clearInstanceForTest()
com.google.android.gms.common.api.internal.RegisterListenerMethod:
    1:5:protected RegisterListenerMethod(com.google.android.gms.common.api.internal.ListenerHolder)
com.google.android.gms.common.data.DataBufferRef:
    5:5:protected int getDataRow()
    12:12:protected long getLong(java.lang.String)
    13:13:protected int getInteger(java.lang.String)
    14:14:protected boolean getBoolean(java.lang.String)
    15:15:protected java.lang.String getString(java.lang.String)
    16:16:protected float getFloat(java.lang.String)
    17:17:protected double getDouble(java.lang.String)
    18:18:protected byte[] getByteArray(java.lang.String)
    19:22:protected android.net.Uri parseUri(java.lang.String)
    23:24:protected void copyToBuffer(java.lang.String,android.database.CharArrayBuffer)
    25:25:protected boolean hasNull(java.lang.String)
com.google.android.gms.common.data.EntityBuffer:
    1:3:protected EntityBuffer(com.google.android.gms.common.data.DataHolder)
com.google.android.gms.common.internal.BaseGmsClient:
    28:47:protected BaseGmsClient(android.content.Context,android.os.Handler,com.google.android.gms.common.internal.GmsClientSupervisor,com.google.android.gms.common.GoogleApiAvailabilityLight,int,com.google.android.gms.common.internal.BaseGmsClient$BaseConnectionCallbacks,com.google.android.gms.common.internal.BaseGmsClient$BaseOnConnectionFailedListener)
com.google.android.gms.common.internal.DowngradeableSafeParcel:
    6:8:protected static java.lang.Integer getUnparcelClientVersion()
    protected abstract boolean prepareForClientVersion(int)
    9:9:protected boolean shouldDowngrade()
    12:13:protected static boolean canUnparcelSafely(java.lang.String)
com.google.android.gms.common.internal.GmsClient:
    1:5:protected GmsClient(android.content.Context,android.os.Handler,int,com.google.android.gms.common.internal.ClientSettings)
    27:34:protected GmsClient(android.content.Context,android.os.Handler,com.google.android.gms.common.internal.GmsClientSupervisor,com.google.android.gms.common.GoogleApiAvailability,int,com.google.android.gms.common.internal.ClientSettings,com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks,com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener)
    43:43:protected final com.google.android.gms.common.internal.ClientSettings getClientSettings()
com.google.android.gms.common.internal.GmsClientEventManager:
    17:19:protected final void onConnectionSuccess()
com.google.firebase.FirebaseApp:
    static final java.lang.String DATA_COLLECTION_DEFAULT_ENABLED
com.google.firebase.components.Lazy:
    33:39:Lazy(java.lang.Object)
    69:69:boolean isInitialized()
com.google.firebase.provider.FirebaseInitProvider:
    static final java.lang.String EMPTY_APPLICATION_ID_PROVIDER_AUTHORITY
com.horcrux.svg.FontData:
    static final double DEFAULT_FONT_SIZE
com.horcrux.svg.FontData$AbsoluteFontWeight:
    static final int normal
com.horcrux.svg.RenderableView:
    static final int CAP_ROUND
    static final int JOIN_ROUND
    static final int FILL_RULE_NONZERO
com.horcrux.svg.TSpanView:
    static final java.lang.String requiredFontFeatures
    static final java.lang.String disableDiscretionaryLigatures
    static final java.lang.String defaultFeatures
    static final java.lang.String additionalLigatures
    static final java.lang.String fontWeightTag
com.horcrux.svg.TextLayoutAlgorithm:
    88:1301:com.horcrux.svg.TextLayoutAlgorithm$CharacterInformation[] layoutText(com.horcrux.svg.TextLayoutAlgorithm$LayoutInput)
com.horcrux.svg.TextLayoutAlgorithm$1CharacterPositioningResolver:
    final synthetic com.horcrux.svg.TextLayoutAlgorithm this$0
    254:275:TextLayoutAlgorithm$1CharacterPositioningResolver(com.horcrux.svg.TextLayoutAlgorithm,com.horcrux.svg.TextLayoutAlgorithm$CharacterInformation[],java.lang.String[],java.lang.String[],java.lang.String[],java.lang.String[])
com.horcrux.svg.TextLayoutAlgorithm$1TextLengthResolver:
    final synthetic com.horcrux.svg.TextLayoutAlgorithm this$0
    573:573:TextLayoutAlgorithm$1TextLengthResolver(com.horcrux.svg.TextLayoutAlgorithm,com.horcrux.svg.TextLayoutAlgorithm$CharacterInformation[])
    573:573:static synthetic void access$000(com.horcrux.svg.TextLayoutAlgorithm$1TextLengthResolver,com.horcrux.svg.TextView)
com.horcrux.svg.TextLayoutAlgorithm$CharacterInformation:
    int index
    double y
    com.horcrux.svg.TextView element
    boolean hidden
    boolean xSpecified
    boolean ySpecified
    boolean rotateSpecified
    final synthetic com.horcrux.svg.TextLayoutAlgorithm this$0
    20:39:TextLayoutAlgorithm$CharacterInformation(com.horcrux.svg.TextLayoutAlgorithm,int,char)
com.horcrux.svg.TextLayoutAlgorithm$LayoutInput:
    com.horcrux.svg.TextView text
    boolean horizontal
    final synthetic com.horcrux.svg.TextLayoutAlgorithm this$0
    42:42:TextLayoutAlgorithm$LayoutInput(com.horcrux.svg.TextLayoutAlgorithm)
com.horcrux.svg.TextPathView:
    77:77:com.horcrux.svg.TextProperties$TextPathMethod getMethod()
    82:82:com.horcrux.svg.TextProperties$TextPathSpacing getSpacing()
com.horcrux.svg.VirtualView:
    static final float MIN_OPACITY_FOR_DRAW
    static final int CLIP_RULE_NONZERO
com.reactnativecommunity.asyncstorage.ReactDatabaseSupplier:
    static final java.lang.String TABLE_CATALYST
    static final java.lang.String KEY_COLUMN
    static final java.lang.String VALUE_COLUMN
    static final java.lang.String VERSION_TABLE_CREATE
io.invertase.firebase.database.RNFirebaseTransactionHandler:
    95:95:java.util.Map getUpdates()
io.invertase.firebase.firestore.RNFirebaseFirestoreCollectionReference:
    152:152:boolean hasListeners()
io.invertase.firebase.firestore.RNFirebaseFirestoreDocumentReference:
    224:224:boolean hasListeners()
io.invertase.firebase.notifications.RNFirebaseNotificationManager:
    static final java.lang.String SCHEDULED_NOTIFICATION_EVENT
okio.Buffer:
    static final int REPLACEMENT_CHARACTER
    1673:1679:java.util.List segmentSizes()
okio.Segment:
    static final int SIZE
    static final int SHARE_MINIMUM
okio.SegmentPool:
    static final long MAX_SIZE
