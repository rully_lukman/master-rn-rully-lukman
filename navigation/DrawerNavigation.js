import React from 'react'
import { Platform, Dimensions } from 'react-native'
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation'

import HomeScreen from '../screens/HomeScreen'
import LinksScreen from '../screens/LinksScreen'
import SettingsScreen from '../screens/SettingsScreen'
import MenuDrawer from '../components/MenuDrawer'
import LoginScreen from '../screens/LoginScreen'
import ProductScreen from '../screens/ProductScreen'
import AuthLoadingScreen from '../screens/AuthLoadingScreen'
import PushNotifScreen from '../components/PushNotification'
import DetailProductScreen from '../screens/DetailProductScreen'
import RegisterScreen from '../screens/RegisterScreen'
import TestChatScreen from '../screens/TestChatScreen';

const WIDTH = Dimensions.get('window').width

const DrawerConfig = {
    drawerWidth: WIDTH * 0.83,
    contentComponent: ({ navigation }) => {
        return (
            <MenuDrawer navigation={navigation} />
        )
    }
}

const loginNavigator = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
        },
        Register: {
            screen: RegisterScreen
        }
    },

)

const productNavigator = createStackNavigator(
    {
        Products: {
            screen: ProductScreen
        },
        DetailProduct: {
            screen: DetailProductScreen
        }
    }
)

const DrawerNavigator = createDrawerNavigator(
    {
        Home: {
            screen: HomeScreen
        },
        productNavigator,
        Links: {
            screen: LinksScreen
        },
        Settings: {
            screen: SettingsScreen
        },
        TestChat: {
            screen: TestChatScreen
        },
        PushNotif: {
            screen: PushNotifScreen
        }
    },
    DrawerConfig
)

const switchNavigator = createSwitchNavigator(
    {
        AuthLoadingScreen,
        loginNavigator,
        DrawerNavigator
    },
    {
        initialRouteName: 'AuthLoadingScreen',
    }
)

export default createAppContainer(switchNavigator)