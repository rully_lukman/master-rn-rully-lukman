import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList } from 'react-native'
import firebase from 'react-native-firebase'

export default class TestChatScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }


    componentDidMount() {
        this.getConfig()
    }

    async getConfig() {
        // let check = firebase.database().ref('id')
        // // console.log(check)
        // const ref = firebase.database().ref('path');
        // ref.update({ foo: 'bar' });
        // const ref1 = firebase.database().ref('path1/user');
        // ref1.update({ id: '100' });
        // const ref2 = firebase.database().ref('chatroom/room1/');
        // ref2.update({ id: '100', chat: 'hi' });
        // ref.push({ id: '200', chat: 'hi 200' });

        // let snapshot = await ref.once('foo')
        // console.log('Id user : ' + snapshot.val())


        // // push data to users key
        // const ref = firebase.database().ref(`/users`);

        // await ref.push({
        //     uid: 2,
        //     name: 'Blurry Eyes',
        //     role: 'customer',
        // });


        // Create a reference
        const ref = firebase.database().ref(`/users/`);

        // Fetch the data snapshot
        const snapshot = await ref.once('value');

        // console.log('User data: ', snapshot.val());
        let fetchData = snapshot.val()

        this.setState({
            lists: fetchData
        })

        // const ref = firebase.database().ref(`/users/`);
        // console.log(ref.once('value', onSnapshot))



    }
    render() {
        let { lists } = this.state
        console.log(lists)
        return (
            <View style={styles.container}>
                <Text> textInComponent </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
