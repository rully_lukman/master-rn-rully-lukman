import React from 'react'
import { Text, StyleSheet, View, Platform, TouchableOpacity, Image, ScrollView, Dimensions, Button } from 'react-native'
import Icon from 'react-native-ionicons'
import QRCode from "react-native-qrcode-svg"
import Modal from 'react-native-modalbox'
import Share from 'react-native-share'
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'rn-fetch-blob'

export default class DetailProductScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            items: {
                id: '',
                name: '',
                description: '',
                price: '',
                thumbnail: 'https://upload.wikimedia.org/wikipedia/commons/5/59/Empty.png'
            },
            idProduct: '',
            photo: 400,
            isModalVisible: false,
            qrvalue: 'http://blackbone23projects.xyz',

        }
    }

    async componentWillMount() {
        // await this.getProductId()
        // await this.paramData()
    }

    async componentDidMount() {
        await this.fetchProduct()
        await this.getProductId()
        await this.photoDimension()
    }

    async photoDimension() {
        const WIDTH = Dimensions.get('window').width
        const HEIGHT = Dimensions.get('window').height

        if (WIDTH < HEIGHT) {
            console.log('portrait')
            this.setState({
                photo: WIDTH * 0.9
            })
        } else {
            console.log('landscape')
            this.setState({
                photo: HEIGHT * 0.9
            })
        }

        console.log('ukuran foto ' + this.state.photo)
    }

    async getProductId() {
        // let { navigation } = this.props
        let idProduct = this.props.navigation.getParam('idProduct', null)
        console.log('id produk : ' + idProduct)
    }

    async fetchProduct() {
        try {
            await fetch('http://api2.blackbone23projects.xyz/product/findId/' + this.state.idProduct, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: '',
            })
                .then(response => response.text())
                .then(checker => {
                    this.handlerProduct(checker)
                })

        } catch (error) {
            alert(error)
        }
    }

    handlerProduct(checker) {
        let firstString = checker.charAt(0)
        if (firstString === "{") {
            let responseJson = JSON.parse(checker)
            console.log(responseJson.data.name)
            this.setState({
                items: responseJson.data
            })
        } else {
            this.fetchProduct()
        }
    }

    static navigationOptions = {
        headerTitle: (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ fontSize: 20, fontWeight: '900' }}>Detail Product</Text>
            </View>
        ),
        headerRight: (
            <View style={{ marginRight: 20, }}>
                <Text></Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: 'orange',
            borderBottomColor: 'orange',
            borderBottomWidth: 3,
        }

    }

    async _goBack(key) {
        this.props.navigation.navigate(key)
    }

    convertIDR = (angka) => {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
        return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    onClose() {
        console.log('Modal just closed');
    }

    onOpen() {
        console.log('Modal just opened');
    }

    _share() {
        let fileUrl = this.state.items.thumbnail
        let type = 'image/jpeg'
        let filePath = null;
        const configOptions = { fileCache: true };
        RNFetchBlob.config(configOptions)
            .fetch('GET', fileUrl)
            .then(resp => {
                filePath = resp.path();
                // console.log(resp.readFile('base64'))
                return resp.readFile('base64');
            })
            .then(async base64Data => {
                base64Data = 'data:' + type + ';base64,' + base64Data;
                console.log(base64Data)
                await Share.open({
                    message: 'Hello World',
                    url: base64Data,
                    type: type
                });
                // remove the image or pdf from device's storage
                await RNFS.unlink(filePath);
            });
    }

    render() {
        let { navigation } = this.props
        let idProduct = navigation.getParam('idProduct', null)
        this.state.idProduct = idProduct
        let { items } = this.state


        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.shareLink}
                    onPress={() => this._share()}
                >
                    <Icon
                        ios="ios-share"
                        android="md-share"
                        color="green"
                    />
                </TouchableOpacity>
                {/* <View style={styles.header}>
                    <View style={styles.headerContainer}>
                        <View style={styles.backButtonContainer}>
                            <TouchableOpacity onPress={() => this._goBack(keyBack)}>
                                <Icon
                                    style={styles.backButton}
                                    ios="ios-arrow-back"
                                    android="md-arrow-back"
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headerTitleContainer}>
                            <View style={styles.androidTitle}>
                                <Text style={styles.headerTitle}>Title</Text>
                            </View>
                        </View>
                    </View>
                </View> */}
                <View style={{ flex: 90, }}>
                    <ScrollView style={styles.containerBody}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleBody}>{items.name}</Text>
                        </View>
                        <View style={styles.imageAlign}>
                            <Image
                                source={{ uri: items.thumbnail }}
                                style={{ width: this.state.photo, height: this.state.photo, }}
                            // style={{ width: 350, height: 350, }}
                            />
                        </View>
                        <View style={styles.priceContainer}>
                            <Text style={styles.price}>Price : {this.convertIDR(items.price)}</Text>
                        </View>
                        <View style={styles.descContainer}>
                            <Text style={styles.desc}>{items.description}</Text>
                        </View>

                    </ScrollView>
                </View>
                <View style={{ flex: 10, alignItems: 'center', justifyContent: 'center', }}>
                    {/* <TouchableOpacity onPress={this.toggleModal}> */}
                    <TouchableOpacity onPress={() => this.refs.modal4.open()}>
                        <Text style={styles.modalToggle}>Redeem Your Code</Text>
                    </TouchableOpacity>
                </View>
                <Modal style={[styles.modal, styles.modalContent]} position={"bottom"} ref={"modal4"}>
                    <View style={styles.modalScale}>
                        <View style={styles.modalContainer}>
                            <QRCode
                                size={250}
                                value={this.state.qrvalue}
                            />
                            <View style={styles.modalTextContainer}>
                                <Text style={styles.modalText}>Show this QRCode to our Front Office to Redeem your Voucher.</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 70,
        backgroundColor: 'orange',
    },
    headerContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    backButtonContainer: {
        width: 50,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backButton: {
        marginLeft: 20,
    },
    headerTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    androidTitle: {
        marginRight: Platform.OS === 'ios' ? 0 : 60,
    },
    headerTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
    },
    containerBody: {
        flex: 1,
        padding: 20,
    },
    imageAlign: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    titleContainer: {
        alignItems: 'flex-start',
        marginBottom: 20,
    },
    titleBody: {
        fontSize: 20,
        fontWeight: '900',
        color: '#000',
    },
    priceContainer: {
        alignItems: 'flex-start',
        marginBottom: 10,
    },
    price: {
        fontSize: 16,
    },
    descContainer: {

    },
    desc: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalScale: {
        height: '80%',
        backgroundColor: 'white',
        padding: 10,
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalTextContainer: {
        padding: 20,
        textAlign: 'center',
    },
    modalText: {
        textAlign: 'center',
        fontSize: 16,
    },
    modalToggle: {
        fontSize: 18,
        fontWeight: '900',
        color: 'orange',
    },
    modalContent: {
        height: 500
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: "black",
        fontSize: 22
    },
    shareLink: {
        position: 'absolute',
        bottom: 100,
        right: 30,
        zIndex: 9,
    },
})
