import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props)
        this.routingScreen()

        this.state = {

        }
    }

    routingScreen = async () => {
        const userData = await AsyncStorage.getItem('user')
        this.props.navigation.navigate(userData ? 'DrawerNavigator' : 'loginNavigator');
    }

    render() {
        return (
            <View>

            </View>
        )
    }
}

const styles = StyleSheet.create({})
