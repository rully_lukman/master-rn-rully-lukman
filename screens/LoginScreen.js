import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput, SafeAreaView } from 'react-native'
import md5 from 'md5'
import AsyncStorage from '@react-native-community/async-storage'
import Loading from 'react-native-whc-loading'

export default class LoginScreen extends Component {

    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: ''
        }
    }

    loginAction = async () => {
        console.log('your data is, username : ' + this.state.username + ' and password : ' + md5(this.state.password))

        this.refs.loading.show()

        try {
            await fetch('http://api2.blackbone23projects.xyz/user/login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: '{"username" : "' + this.state.username + '","password": "' + md5(this.state.password) + '"}',
            })
                // display response as text
                .then(response => response.text())
                // execute response
                .then(responseJson => {
                    this.loginHandler(responseJson)
                })
        } catch (error) {
            this.refs.loading.close()
            alert(error)
        }
    }

    loginHandler(responseJson) {
        //check response is json or html 
        let firstString = responseJson.charAt(0)
        if (firstString === "<") {
            console.log('return html on fetch (error) : ' + firstString)
            this.loginAction()
        } else {
            console.log('return json on fetch (success) : ' + firstString)
            let jsonFetch = JSON.parse(responseJson)
            let status = jsonFetch.status
            if (status === "200") {
                this.refs.loading.close()
                console.log("login success, your asyc data is (stringify) : " + JSON.stringify(jsonFetch.data))
                AsyncStorage.setItem('user', JSON.stringify(jsonFetch.data))
                this.navigateLogin()
            } else {
                this.refs.loading.close()
                alert(jsonFetch.data)
                console.log(status)
            }
        }
    }

    navigateLogin = () => {
        this.props.navigation.navigate('Home')
    }

    registerAction = () => {
        this.props.navigation.navigate('Register', { title: 'Register Member' })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View>
                    <Text style={styles.text}> Login Screen  </Text>
                    <TextInput
                        style={styles.textinputbody}
                        autoCapitalize="none"
                        placeholder="username"
                        onChangeText={
                            username => this.setState({
                                username: username
                            })
                        }
                        value={this.state.username}
                    />

                    <TextInput
                        style={styles.textinputbody}
                        autoCapitalize="none"
                        placeholder="password"
                        secureTextEntry={true}
                        onChangeText={
                            password => this.setState({
                                password: password
                            })
                        }
                        value={this.state.password}
                    />
                    <View style={styles.buttons}>
                        <TouchableOpacity
                            style={styles.loginBtn}
                            onPress={this.loginAction}
                        >
                            <Text>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.registerAction}
                            style={styles.regisBtn}
                        >
                            <Text>Register</Text>
                        </TouchableOpacity>
                    </View>
                    <Loading ref="loading" />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 25,
    },
    textinput: {
        flex: 1,
        justifyContent: 'center',
    },
    textinputbody: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 2,
        margin: 5,
    },
    buttons: {
        flexDirection: 'row',
    },
    loginBtn: {
        backgroundColor: 'green',
        padding: 10,
        margin: 10,
        borderRadius: 4,
    },
    regisBtn: {
        backgroundColor: 'orange',
        padding: 10,
        margin: 10,
        borderRadius: 4,
    },
})
