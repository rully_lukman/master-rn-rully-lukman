import React from 'react'
import { Text, StyleSheet, View, TextInput, TouchableOpacity, SafeAreaView } from 'react-native'
import md5 from 'md5'
import Loading from 'react-native-whc-loading'
import RNBottomActionSheet from 'react-native-bottom-action-sheet'

export default class RegisterScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            fullname: '',
            username: '',
            email: '',
            password: ''
        }
    }


    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ fontSize: 20, fontWeight: '900' }}>{navigation.getParam('title', 'Void')}</Text>
                </View>
            ),
            headerRight: (
                <View style={{ marginRight: 20, }}>
                    <Text></Text>
                </View>
            ),
            headerStyle: {
                backgroundColor: 'orange',
                borderBottomColor: 'orange',
                borderBottomWidth: 3,
            }
        }

    }

    regisAction = async () => {
        console.log('your data is, fullname : ' + this.state.fullname + ', username : ' + this.state.username + ', email : ' + this.state.email + ' and password : ' + md5(this.state.password))

        this.refs.loading.show()

        try {
            await fetch('http://api2.blackbone23projects.xyz/user/register', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: '{"fullname" : "' + this.state.fullname + '","username" : "' + this.state.username + '","email" : "' + this.state.email + '","password": "' + md5(this.state.password) + '"}',
            })
                // display response as text
                .then(response => response.text())
                // execute response
                .then(responseJson => {

                    // setTimeout(() => {
                    //     this.refs.loading.close()
                    // }, 2000)

                    // setTimeout(() => {
                    //check response is json or html 
                    this.handleRegister(responseJson)
                    // }, 2100)

                })
        } catch (error) {
            this.refs.loading.close()
            alert(error)
        }
    }

    handleRegister(responseJson) {
        let firstString = responseJson.charAt(0)
        if (firstString === "<") {
            console.log('return html on fetch (error) : ' + firstString)
            this.regisAction()
        } else {
            console.log('return json on fetch (success) : ' + firstString)
            let jsonFetch = JSON.parse(responseJson)
            let status = jsonFetch.status
            if (status === "200") {
                this.refs.loading.close()
                console.log(JSON.stringify(jsonFetch.data))
                alert(jsonFetch.data)
                this.navigateToLogin()
            } else {
                alert(jsonFetch.data)
                console.log(status)
            }
        }
    }

    navigateToLogin = () => {
        this.props.navigation.navigate('Login')
    }

    redirectLogin = () => {
        this.props.navigation.navigate('Login')
    }

    confirmRegister = () => {
        let AlertView = RNBottomActionSheet.AlertView
        AlertView.Show({
            title: "Confirmation",
            message: "Proceed Your Registration Data ?",
            positiveText: "OK",
            positiveBackgroundColor: "green",
            positiveTextColor: "white",
            negativeText: "Exit",
            negativeBackgroundColor: "red",
            negativeTextColor: "red",
            theme: 'light',
            onPositive: () => {
                console.log('positive clicked')
                this.regisAction()
            },
            onNegative: () => {
                console.log('negative clicked')
            }
        })
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View>
                    <Text style={styles.text}> Register New Member  </Text>
                </View>
                <View>
                    <TextInput
                        style={styles.textinputbody}
                        autoCapitalize="none"
                        placeholder="fullname"
                        onChangeText={
                            fullname => this.setState({
                                fullname: fullname
                            })
                        }
                        value={this.state.fullname}
                    />

                    <TextInput
                        style={styles.textinputbody}
                        autoCapitalize="none"
                        placeholder="username"
                        onChangeText={
                            username => this.setState({
                                username: username
                            })
                        }
                        value={this.state.username}
                    />

                    <TextInput
                        style={styles.textinputbody}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        placeholder="email"
                        onChangeText={
                            email => this.setState({
                                email: email
                            })
                        }
                        value={this.state.email}
                    />

                    <TextInput
                        style={styles.textinputbody}
                        autoCapitalize="none"
                        placeholder="password"
                        secureTextEntry={true}
                        onChangeText={
                            password => this.setState({
                                password: password
                            })
                        }
                        value={this.state.password}
                    />
                    <View style={styles.buttons}>
                        <TouchableOpacity
                            style={styles.regisBtn}
                            onPress={this.confirmRegister}
                        >
                            <Text>Register</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.redirectLogin}
                            style={styles.backBtn}
                        >
                            <Text>Back</Text>
                        </TouchableOpacity>
                    </View>
                    <Loading ref="loading" />
                </View >
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 25,
    },
    textinput: {
        flex: 1,
        justifyContent: 'center',
    },
    textinputbody: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 2,
        margin: 5,
    },
    buttons: {
        flexDirection: 'row',
    },
    regisBtn: {
        backgroundColor: 'orange',
        padding: 10,
        margin: 10,
        borderRadius: 4,
    },
    backBtn: {
        backgroundColor: 'red',
        padding: 10,
        margin: 10,
        borderRadius: 4,
    },
})
