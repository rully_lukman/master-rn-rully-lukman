import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, FlatList, TouchableOpacity, Image, Dimensions } from 'react-native'
import MenuButton from '../components/MenuButton'
import AsyncStorage from '@react-native-community/async-storage'


export default class ProductScreen extends React.Component {

    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)

        this.state = {


        }
    }


    async componentDidMount() {
        await this.getProducts()
    }

    async getProducts() {
        try {
            await fetch('http://api2.blackbone23projects.xyz/product/getAll', {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: '',
            })
                .then(response => response.text())
                .then(responseJson => {
                    this.fetchProducts(responseJson)
                })
        } catch (error) {
            console.log(error)
        }
    }

    fetchProducts(responseJson) {
        let firstString = responseJson.charAt(0)
        if (firstString === "<") {
            console.log('return html on fetch (error) : ' + firstString)
            this.getProducts()
        } else {
            console.log('return json on fetch (success) : ' + firstString)
            let jsonFetch = JSON.parse(responseJson)
            let status = jsonFetch.status
            if (status === "200") {
                this.setState({ items: jsonFetch.data })
                // console.log(jsonFetch.data)
            } else {
                console.log(jsonFetch.data)
            }
        }
    }


    _onPress = async (id) => {
        console.log('id hit : ' + id)
        this.props.navigation.navigate('DetailProduct', { key: 'Products', idProduct: id })
    }

    render() {
        const { items } = this.state
        return (
            <View style={styles.basecontainer}>
                <View style={styles.header}>
                    <MenuButton navigation={this.props.navigation} />
                </View>
                <Container>
                    <ScrollView>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <FlatList
                                data={items}
                                keyExtractor={(item) => item.id}
                                renderItem={({ item, i }) =>
                                    <TouchableOpacity onPress={() => this._onPress(item.id)}>
                                        <Card key={i}>
                                            <View style={styles.innercard}>
                                                <Image source={{ uri: item.thumbnail }} style={styles.image} />
                                                <View style={styles.descContainer}>
                                                    <Text style={styles.title}>{item.name}</Text>
                                                    <Text style={styles.description}>{item.description}</Text>
                                                    <View style={styles.priceContainer}>
                                                        <Text style={styles.price}>Rp. {item.price}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.arrowNav}>
                                                    <Text>></Text>
                                                </View>
                                            </View>
                                        </Card>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                    </ScrollView>
                </Container>
            </View>
        )
    }
}

class Container extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                {this.props.children}
            </View>
        )
    }
}

class Card extends React.Component {
    render() {
        return (
            <View style={styles.cardContainer}>
                <View style={styles.card}>
                    {this.props.children}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    basecontainer: {
        flex: 1
    },
    header: {
        height: 55,
        backgroundColor: 'orange',
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    cardContainer: {
        flex: 1,
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        flex: 1,
        height: '100%',
        width: '100%',
        elevation: 3,
        borderWidth: 0.5,
        borderColor: '#ccc',
        padding: 10,
    },
    innercard: {
        flexDirection: 'row',
    },
    image: {
        width: '30%',
        height: 150,
        resizeMode: 'stretch',
    },
    title: {
        fontSize: 13,
        fontWeight: 'bold',
    },
    description: {
        fontSize: 10,
        paddingTop: 10,
    },
    descContainer: {
        width: '60%',
        paddingLeft: 10,
    },
    priceContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingLeft: 10,
        bottom: 10,
    },
    price: {
        fontSize: 13,
        fontWeight: 'bold',
    },
    arrowNav: {
        flex: 1,
        flexDirection: 'column-reverse',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginRight: 5,
    },
})
