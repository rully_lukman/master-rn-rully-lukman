import React from 'react'
import { View, Text, StyleSheet, Platform, Button, Dimensions, TouchableOpacity, Image, ScrollView } from 'react-native'
import Loading from 'react-native-whc-loading'
import AsyncStorage from '@react-native-community/async-storage'
import PushNotification from '../components/PushNotification'


const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height


export default class MenuDrawer extends React.Component {

    componentWillMount = async () => {
        let user = JSON.parse(await AsyncStorage.getItem('user'))
        if (user === null) {
            this.props.navigation.navigate('Login')
        }
    }

    constructor(props) {
        super(props)

        this.state = {
            fullname: '',
            username: ''
        }
    }

    componentDidMount = async () => {
        this._metaData()
    }

    _metaData = async () => {
        AsyncStorage.getItem('user', (error, result) => {
            if (result) {
                let resultParsed = JSON.parse(result)
                this.setState({
                    fullname: resultParsed.fullname,
                    username: resultParsed.username
                });

            }
        });
    }

    navLink(nav, text) {
        return (
            <TouchableOpacity
                style={{ height: 50 }}
                onPress={() => this.props.navigation.navigate(nav)}
            >
                <Text style={styles.link}>{text}</Text>
            </TouchableOpacity>
        )
    }

    logout = async () => {
        this.refs.loading.show()

        //send to logout API
        let user = JSON.parse(await AsyncStorage.getItem('user'))
        console.log('attempt logout for user id : ' + user.id)

        try {
            await fetch('http://api2.blackbone23projects.xyz/user/logout', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: '{"id" : "' + user.id + '"}',
            })
                .then(response => response.text())
                .then(responseJson => {
                    console.log(responseJson)
                    this.logoutHandler(responseJson)
                })
        } catch (error) {
            this.refs.loading.close()
            alert(error)
        }
    }

    logoutHandler(responseJson) {
        //check response is json or html 
        let firstString = responseJson.charAt(0)
        if (firstString === "<") {
            console.log('return html : ' + firstString)
            this.logout()
        } else {
            console.log('return json' + firstString)
            let jsonFetch = JSON.parse(responseJson)
            let status = jsonFetch.status
            //if logout success
            if (status === "200") {
                this.refs.loading.close()
                // clear local storage
                AsyncStorage.removeItem('user')
                alert(jsonFetch.data)
                //navigate to login screen
                this.navigateLogout()
            } else {
                alert(jsonFetch.data)
                console.log(status)
            }
        }
    }

    navigateLogout = () => {
        this.props.navigation.navigate('Login')
    }

    render() {
        return (
            <View style={styles.container}>
                <PushNotification />
                <ScrollView style={styles.scrollview}>
                    <View style={styles.topLinks}>
                        <View style={styles.profile}>
                            <View style={styles.imgView}>
                                <Image style={styles.img} source={require('../assets/profile.jpg')} />
                            </View>
                            <View style={styles.profileText}>
                                <Text style={styles.name}>{this.state.fullname}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.bottomLinks}>
                        {this.navLink('Home', 'Home')}
                        {this.navLink('Products', 'Products')}
                        {this.navLink('Links', 'Links')}
                        {this.navLink('Settings', 'Settings')}
                        {this.navLink('TestChat', 'TestChat')}
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity
                        onPress={this.logout}
                    >
                        <Text style={styles.description}>Logout</Text>
                    </TouchableOpacity>
                    <Text style={styles.version}>v1.0</Text>
                </View>
                <Loading ref="loading" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollview: {
        flex: 1,
    },
    profile: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777',
    },
    profileText: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    name: {
        fontSize: 20,
        paddingBottom: 5,
        color: 'white',
        textAlign: 'left',
    },
    img: {
        height: 70,
        width: 70,
        borderRadius: 50,

    },
    imgView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    topLinks: {
        height: 160,
        backgroundColor: 'black',
    },
    bottomLinks: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 450,
    },
    link: {
        flex: 1,
        fontSize: 20,
        padding: 6,
        paddingLeft: 14,
        margin: 5,
        textAlign: 'left',
    },
    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgrey',
    },
    description: {
        flex: 1,
        marginTop: 15,
        marginLeft: 20,
        fontSize: 16,
        color: 'red',
    },
    version: {
        flex: 1,
        textAlign: 'right',
        marginRight: 20,
        color: 'grey'
    },
})
