import React from 'react'
import { View, Alert, Text } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import firebase from 'react-native-firebase'
import { withNavigation } from 'react-navigation'

// export default class PushNotification extends React.Component {
class PushNotification extends React.Component {

    async componentDidMount() {
        this.checkPermission();
        this.createNotificationListeners(); //add this line
    }

    componentWillUnmount() {
        this.notificationListener;
        this.notificationOpenedListener;
    }


    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
            console.log('permission enabled')
        } else {
            this.requestPermission();
            console.log('permission disabled')
        }
    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
            console.log('user authorized')
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected')
        }
    }

    //3
    async getToken() {
        AsyncStorage.getItem('user', (error, result) => {
            if (result) {
                let resultParsed = JSON.parse(result)
                let tokenServer = resultParsed.token
                let idMember = resultParsed.id
                // console.log('token server : ' + tokenServer)
                if (tokenServer !== undefined) {
                    this.refreshToken(tokenServer, idMember)
                } else {
                    alert('There is no token in your localstorage, please check your API login data response or your local storage')
                }
            }
        });
    }

    //4
    async refreshToken(tokenServer, idMember) {
        let tokenDevice = await firebase.messaging().getToken();
        console.log('token device : ' + tokenDevice)
        console.log('id member : ' + idMember)
        if (tokenServer === tokenDevice) {
            console.log('token server = token device')
        } else {
            console.log('token server != token device, need to be updated')
            try {
                await fetch('http://api2.blackbone23projects.xyz/notification/refreshToken', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: '{"id" : "' + idMember + '", "token" : "' + tokenDevice + '"}',
                })
                    .then(response => response.text())
                    .then(responseJson => {
                        this.handlerRefreshToken(responseJson)
                    })

            } catch (error) {
                console.log('error retreive token from API');
            }
        }
    }

    handlerRefreshToken(responseJson) {
        let firstString = responseJson.charAt(0)
        if (firstString === "<") {
            console.log('return html on fetch (error) : ' + firstString)
            this.getToken()
        } else {
            console.log('return json on fetch (success) : ' + firstString)
            let jsonFetch = JSON.parse(responseJson)
            let status = jsonFetch.status
            if (status === "200") {
                console.log(jsonFetch.data)
                // AsyncStorage.setItem('user', JSON.stringify(jsonFetch.data))
            } else {
                console.log(status)
                console.log(jsonFetch.data)
                this.getToken()
            }
        }
    }

    _navigateNotif = async (nav) => {
        this.props.navigation.navigate(nav)
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const { title, body } = notification;
            console.log('onNotification:' + title);

            const localNotification = new firebase.notifications.Notification({
                sound: 'tothepoint.wav',
                show_in_foreground: true,
            })
                .setSound('tothepoint.wav')
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body)
                .setData(notification.data)
                .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
                .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
                .android.setColor('#000000') // you can set a color here
                .android.setPriority(firebase.notifications.Android.Priority.High);

            firebase.notifications()
                .displayNotification(localNotification)
                .catch(err => console.error(err));
        });

        const channel = new firebase.notifications.Android.Channel('fcm_FirebaseNotifiction_default_channel', 'Demo app name', firebase.notifications.Android.Importance.High)
            .setDescription('Demo app description')
            .setSound('tothepoint.wav');
        firebase.notifications().android.createChannel(channel);

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body, nav } = notificationOpen.notification.data;
            console.log('onNotificationOpened:' + title);
            // console.log(notificationOpen.action)
            // Alert.alert(title, body)
            // Alert.alert('Will be Redirect To:', nav)
            firebase.notifications().removeAllDeliveredNotifications()
            this._navigateNotif(nav)
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const { title, body, nav } = notificationOpen.notification.data;
            console.log('getInitialNotification:');
            // Alert.alert('Will be Redirect To:', nav)
            firebase.notifications().removeAllDeliveredNotifications()
            this._navigateNotif(nav)
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log("JSON.stringify:", JSON.stringify(message));
        });
    }


    render() {
        return (
            <View>
            </View>
        )
    }
}

export default withNavigation(PushNotification);

