# Master RN rully lukman

My master react native modules : 

compilation of all function that will run on my react native projects, see future change on readme.md or commit status:


## Current status repository

### Release Mark (Android Only)

1. Use react native version 0.59.0
2. Minimum sized apk
3. Use usesCleartextTraffic = true to allow new android version to execute non secure API request (using http)
4. Has installed and use react-native-navigation & react-native-gesture-handler pluggin
5. Has installed react-native-ionicons pluggin
6. Has installed and use react-native-whc-loading pluggin for loading request to API
7. Has installed react-native-share pluggin
8. Has installed and use md5 npm pluggin for password hash
9. Has installed and use @react-native-community/async-storage pluggin to replace native asyncstorage for rn project (native asyncstorage for rn will be deprecated later)
10. Has include privat keystore to release build apk
11. Deploy connection to my private API http://api2.blackbone23projects.xyz/
12. Deploy login auth
13. Deploy basic dashboard with simple navigation with drawer (exclude header, bottom navigation and modal screen)
14. Deploy prevent fail to connect to API, change response from json to text first, then check it (also parse it to json if it's detect as json)
15. Add blank screen for initiate screen for solution async transition checker (login session on react native will pass all async function to render it first, must enchance it to render checker later -> more and more R&D)
16. Move google api to my backup account (rullyagra1@gmail.com)
17. Has installed firebase pluggin and configuration (cloud messaging & push notification)
18. Separate Notification into a component & render it to menu drawer (screen that will reload on each navigation).. notification config will load after login success
19. Setup firebase notification including : 
    - Icon (meta on androidmanifest)
    - Sound (meta on androidmanifest)
    - Notification channel (meta on androidmanifest)
    - Token Device (get, refresh)
    - 3 way notification (foreground, backgroun, on app closed)
20. Unlock proguard rules (disable all warning) // must review for future updates
21. Include background messaging script on bgMessaging.js
22. Add product list screen like hotel room list with fetch
23. Fixing problem using http API from react native 0.59.0 with API 29
24. Remove temporary blank screen, implement AuthLoadingScreen as first screen
25. Add navigation after notification opened reference : https://medium.com/@ashleywnj/react-navigation-cannot-read-property-navigate-of-undefined-94ff5e9d3c2a
26. Detail product screen as stacknavigator (use header to set back to product list)
27. Separate product list and product detail as stack navigator, then put it to drawernavigator
28. Modify menu button styling same as header stacknavigator
29. Get navigation param on static navigationOptions (see LoginScreen and RegisterScreen)
30. RegisterScreen is up
31. Refresh Token with errorhandler (if failed to connect to API)
32. Add logout handler in MenuDrawer.js (logout process)
33. Add login handler in LoginScreen.js
34. Restyling DetailProductScreen.js for UI
35. IDR converter from string on DetailProductScreen.js
36. Has Installed react-native-modalbox (smooth animation)
37. Has remove react-native-modal due UI animation problem
38. Has Installed react-native-qrcode-svg for barcode builder
39. Has Installed & configured react-native-share for share a link / attachment
40. Has Installed & configured rn-fetch-blob for base64 attachment
41. Configure Shared Link on DetailProductScreen.js
42. Temporary remove whc-loading script from Login & Logout process
43. Mark Release on masterrn04.apk
44. Has Installed react-native-bottom-action-sheet
45. Disable whc-loading on RegisterScreen.js
46. Enhancement whc-loading to realtime loadingbar display
47. Fix bug loading not close if login failed response is get from API

### Development Mark, Still updated (Android Only)
1. Develop realtime database for live chat



## React Native Dependencies / Plugins

##### 1. REACT NATIVE SHARE

Reference : https://github.com/react-native-community/react-native-share

Version : 1.2.1


- Don’t Install react-native-fetch-blob
- Install rn-fetch-blob . Reference : https://github.com/joltup/rn-fetch-blob
- On script see on react-native-fetch-blob (still can used on share pdf file with base 64). Reference : https://github.com/joltup/rn-fetch-blob
- Don’t do implementation of FileProvider


##### 2. RN FETCH BLOB

version : 0.10.15

Since react-native-fetch-blob is deprecated, now we use rn-fetch-blob

Reference : https://github.com/joltup/rn-fetch-blob

See notes REACT NATIVE SHARE for general used


##### 3. REACT NATIVE MODAL BOX

version : 1.7.1

Reference : https://www.npmjs.com/package/react-native-modalbox
Example : https://github.com/maxs15/react-native-modalbox/blob/master/Example/index.ios.js


##### 4. REACT NATIVE BOTTOM ACTION SHEET

version : 0.0.23

Reference : https://www.npmjs.com/package/react-native-bottom-action-sheet


##### 5. REACT NATIVE QRCODE

version : 
    - react-native-svg: 9.6.2
    - react-native-qrcode-svg: 5.1.2

Reference : https://github.com/awesomejerry/react-native-qrcode-svg


##### 6. REACT NATIVE ASYNCSTROAGE

version : 1.6.1

Reference : https://github.com/react-native-community/async-storage


##### 7. REACT NATIVE WHC LOADING

version : 1.0.3

Reference : https://github.com/netyouli/react-native-whc-loading


##### 8. REACT NATIVE MD5

version : 2.2.1

Reference : https://www.npmjs.com/package/md5


##### 9. REACT NATIVE QRCODE SCANNER

Reference : https://reactnativecode.com/qr-code-scanner-app-using-camera/


##### 10. React Native Ionicons

version : 4.6.1

Reference : https://www.npmjs.com/package/react-native-ionicons

IconName : https://ionicons.com/cheatsheet.html


##### 11. REACT NAVIGATIN ON REACT NATIVE 0.59.0

version : 
    - react-navigation : 3.11.1
    - react-native-gesture-handler : 1.3.0


    npm install react-navigation --save
    npm install react-native-gesture-handler --save
    react-native link react-native-gesture-handler

Additional on gesture handle must do (if not do this, your drawer can’t close when press on outside the drawer)

Reference : https://reactnavigation.org/docs/en/getting-started.html#installation

To finalise installation of react-native-gesture-handler for Android, be sure to make the necessary modifications to MainActivity.java:


    package com.drawernavigator;

    import com.facebook.react.ReactActivity;

    // add gesture handle
    import com.facebook.react.ReactActivityDelegate;
    import com.facebook.react.ReactRootView;
    import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

    public class MainActivity extends ReactActivity {

        /**
         * Returns the name of the main component registered from JavaScript. This is
         * used to schedule rendering of the component.
         */
        @Override
        protected String getMainComponentName() {
            return "drawernavigator";
        }

        // add gesture handle
        @Override
        protected ReactActivityDelegate createReactActivityDelegate() {
            return new ReactActivityDelegate(this, getMainComponentName()) {
                @Override
                protected ReactRootView createRootView() {
                    return new RNGestureHandlerEnabledRootView(MainActivity.this);
                }
            };
        }
    }


##### 12. REACT NATIVE FIREBASE NOTIFICATION

Reference : https://rnfirebase.io/

on video tutor reference : https://www.youtube.com/watch?v=nHvw1mcaOs0


## Tips & Debug

##### 1. REACT NATIVE BUILD MINIMUM SIZE APK

Reference : https://hashnode.com/post/how-to-reduce-the-app-size-in-react-native-cj6ohocv002rqt9wuy24etkee

##### 2. REACT NATIVE HTTPS ON API 29 ANDROID

on android/app/src/main/res/xml/react_native_config.xml

    <?xml version="1.0" encoding="utf-8"?>
    <network-security-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="false">${your_api_domain}</domain>
    </domain-config>
    </network-security-config>


on AndroidManifest.xml

Under manifest tag : 

    xmlns:tools="http://schemas.android.com/tools"


Under application tag : 


    tools:targetApi="28"
    android:networkSecurityConfig="@xml/react_native_config"
    android:usesCleartextTraffic="true"


##### 3. Task :app:transformClassesAndResourcesWithProguardForRelease FAILED


Try adding this code to your proGuard rule :

    -ignorewarnings
    -keep class * {
        public private *;
    }


Reference : https://stackoverflow.com/questions/41454128/transformclassesandresourceswithproguardforrelease-failed


##### 4. React Navigation Header

Standar header configuration : 

    class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home',
    };

    /* render function, etc */
    }


    Component header configuration : (bila ingin menambahkan logo image di header dan sebagainya)

    class HomeScreen extends React.Component {
    static navigationOptions = {
        headerTitle: <LogoTitle />,
        headerRight: (
        <Button
            onPress={() => alert('This is a button!')}
            title="Info"
            color="#fff"
        />
        ),
    };
    }


    Params header configuration : (bila ingin inject parameter di header)

    class HomeScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
        headerTitle: <LogoTitle />,
        headerRight: (
            <Button
            onPress={navigation.getParam('increaseCount')}
            title="+1"
            color="#fff"
            />
        ),
        };
    };

    componentDidMount() {
        this.props.navigation.setParams({ increaseCount: this._increaseCount });
    }

    state = {
        count: 0,
    };

    _increaseCount = () => {
        this.setState({ count: this.state.count + 1 });
    };

    /* later in the render function we display the count */
    }


##### 5. Generate Relase APK Android (release signed apk)

Reference : https://dev.to/zilurrane/generate-release-mode-apk-for-react-native-project-to-publish-on-playstore-5f78

